PYEX=python3

export PYTHON_VERSION=`$PYEX -c "from distutils import sysconfig; print(sysconfig.get_config_var('VERSION'));"`
echo "Python version is $PYTHON_VERSION"

export PYTHONPATH=$PYTHONPATH:../../csns_lattice/
echo "import package from $PYTHONPATH"
